package com.example.calculator

import android.annotation.SuppressLint
import android.content.res.Configuration
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Valentin Ros Duart.
 * Kotlin Calculator for Android.
 */
class MainActivity : AppCompatActivity() {

    private var operator = ""
    private var period = false
    // The screen text
    private var result = ""
    // Default mode is decimal
    private var mode = "dec"

    /**
     * On start.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // To perform an action depending on the orientation
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            btn_Dec.isEnabled = false
            btn_Dec.setBackgroundColor(Color.parseColor("#8122BD"))
            txtVw_Result.text = ""
            decimalLayout()
        }
    }

    /**
     * Event handler function for onClick on a number.
     * Prints the number/period on the screen and controls possible errors.
     */
    @SuppressLint("SetTextI18n")
    fun btnNumberEvent(view: View) {
        val btnSelected = view as Button
        // The value pressed
        val num = btnSelected.text.toString()
        // The screen text
        result = txtVw_Result.text.toString()
        // If we want to put a period we check
        if (num == ".") {
            // If the period is not already used then write it and update the bool state
            if (!period) {
                // Check if the period can be inserted and if not it adds "0."
                if (result.isEmpty()) {
                    txtVw_Result.text = result + "0" + num
                } else if (operator.isNotEmpty()) {
                    if (result.substringAfterLast(operator).isEmpty()) {
                        txtVw_Result.text = result + "0" + num
                    } else {
                        txtVw_Result.text = result + num
                    }
                } else {
                    txtVw_Result.text = result + num
                }
                period = true
            }
        } else {
            txtVw_Result.text = result + num
        }
    }

    /**
     * Event handler function for onClick on "c".
     * Clears the screen.
     */
    fun btnClearEvent(view: View) {
        // The screen text
        result = txtVw_Result.text.toString()
        if(result.isNotEmpty()) {
            txtVw_Result.text = ""
            operator = ""
            period = false
        }
    }

    /**
     * Event handler function for onClick on an operator.
     * Prints the operator on screen and controls possible errors.
     */
    @SuppressLint("SetTextI18n")
    fun btnOperatorEvent(view: View) {
        val btnSelected = view as Button
        result = txtVw_Result.text.toString()
        // Check if only one operator is entered, is not the first element and is not after a period
        if (operator.isEmpty() && result.isNotEmpty() && !result.endsWith(".")) {
            operator = btnSelected.text.toString()
            // Reset the period state to be able to put it in the new number
            period = false
            txtVw_Result.text = result + operator
        }
    }

    /**
     * Event handler function for onClick on "=".
     * Performs the operation (add/subtract/multiply/divide) and prints the result on the screen
     * and controls possible errors.
     */
    fun btnEqualsEvent(view: View) {
        // When there is text on the screen and the operator is there too we can do the operation
        if (result.isNotEmpty() && operator.isNotEmpty()) {
            result = txtVw_Result.text.toString()
            // Makes sure it can properly access the fist num and the second num when minus is the
            // operation and the sign is set to negative
            // Count the - apparitions in the screen
            val minusCount = result.count { it == "-".single() }
            val numA: String
            val numB: String
            // If there are more than 2 it means the operator and at least one num have a negative
            // sign which means we have to substring in a different way to prevent errors
            if (minusCount >= 2) {
                // Get the operator index by taking the last - sign and getting the one before
                val operatorIndex = (result.indexOfLast { it == "-".single() }) - 1
                numA = result.substring(0, operatorIndex)
                numB = result.substring(operatorIndex + 1, result.length)
            // When there is only the - operator, just take the substring after and before that
            // operator
            } else {
                numA = result.substringBeforeLast(operator)
                numB = result.substringAfterLast(operator)
            }
            // Check if the num variables have values and then do the operations
            if (numA.isNotEmpty() && numB.isNotEmpty()) {
                var a = 0f
                var b = 0f
                when (mode) {
                    "dec" -> {
                        a = numA.toFloat()
                        b = numB.toFloat()
                    }
                    "hex" -> {
                        a = Integer.parseInt(numA, 16).toFloat()
                        b = Integer.parseInt(numB, 16).toFloat()
                    }
                    "bin" -> {
                        a = Integer.parseInt(numA, 2).toFloat()
                        b = Integer.parseInt(numB, 2).toFloat()
                    }
                }

                var res = 0f
                when (operator) {
                    // Add function
                    "+" -> {
                        res = a + b
                    }
                    // Subtract function
                    "-" -> {
                        res = a - b
                    }
                    // Multiply function
                    "*" -> {
                        res = a * b
                    }
                    // Divide function
                    "/" -> {
                        if (b != 0f) {
                            res = a / b
                        } else {
                            Toast.makeText(this, "Can't divide by zero", Toast.LENGTH_LONG).show()
                        }
                    }
                }
                when (mode) {
                    "dec" -> {
                        if (res.toString().endsWith(".0")) {
                            txtVw_Result.text = res.toInt().toString()
                        } else {
                            txtVw_Result.text = res.toString()
                        }
                    }
                    "hex" -> {
                        //txtVw_Result.text = Integer.toHexString(res.toInt())
                        txtVw_Result.text = res.toInt().toString(16).uppercase()
                    }
                    "bin" -> {
                        //txtVw_Result.text = Integer.toBinaryString(res.toInt())
                        txtVw_Result.text = res.toInt().toString(2)
                    }
                }
                // Clear the operator variable and set period to false
                operator = ""
                period = false
            }
        }
    }

    /**
     * Event handler function for onClick on "+/-".
     * Prints the opposite sign on the screen and controls possible errors.
     */
    @SuppressLint("SetTextI18n")
    fun btnToggleSignEvent(view: View) {
        // The screen text
        result = txtVw_Result.text.toString()
        if (result.isNotEmpty()) {
            // If there is no operator in the text the screen text is the number
            if (operator.isEmpty()) {
                // If the number contains a period, negate the sign and show the number as float
                if (result.contains(".")) {
                    var num = result.toFloat()
                    num *= -1
                    txtVw_Result.text = num.toString()
                // If not, negate the sign and show the number as int
                } else {
                    var num = result.toInt()
                    num *= -1
                    txtVw_Result.text = num.toString()
                }
            // If there is an operator in the text we need to get both numbers
            } else {
                // Take the firts number and the second from the screen text
                val numA = result.substringBeforeLast(operator)
                val numB = result.substringAfterLast(operator)
                if (numB.isNotEmpty()) {
                    // If the number contains a period, negate the sign and show the number as float
                    if (result.contains(".")) {
                        var num = numB.toFloat()
                        num *= -1
                        txtVw_Result.text = numA + operator + num
                    // If not, negate the sign and show the number as int
                    } else {
                        var num = numB.toInt()
                        num *= -1
                        txtVw_Result.text = numA + operator + num
                    }
                }
            }
        }
    }

    /**
     * Event handler function for onClick on "%".
     * Divides actual number by 100 and control possible errors.
     */
    @SuppressLint("SetTextI18n")
    fun btnPercentEvent(view: View) {
        // The screen text
        result = txtVw_Result.text.toString()
        if (result.isNotEmpty()) {
            // If there is no operator in the text the screen text is the number
            if (operator.isEmpty()) {
                txtVw_Result.text = (result.toFloat() / 100).toString()
            // If there is an operator in the text we need to get both numbers
            } else {
                val numA = result.substringBeforeLast(operator)
                val numB = result.substringAfterLast(operator)
                if (numB.isNotEmpty()) {
                    txtVw_Result.text = numA + operator + (numB.toFloat() / 100).toString()
                }
            }
        }
    }

    /**
     * Event handler function for onClick on a mode button.
     * Sets the mode.
     */
    @SuppressLint("SetTextI18n")
    fun btnModeEvent(view: View) {
        // The screen text
        val btnSelected = view as Button
        val lastMode = mode
        mode = btnSelected.text.toString().lowercase()
        val defaultColor = Color.parseColor("#018786")
        when (lastMode) {
            "dec" -> {
                btn_Dec.isEnabled = true
                btn_Dec.setBackgroundColor(defaultColor)
            }
            "hex" -> {
                btn_Hex.isEnabled = true
                btn_Hex.setBackgroundColor(defaultColor)
            }
            "bin" -> {
                btn_Bin.isEnabled = true
                btn_Bin.setBackgroundColor(defaultColor)

            }
        }
        txtVw_Result.text = ""
        when (mode) {
            "dec" -> {
                decimalLayout()
            }
            "hex" -> {
                hexadecimalLayout()
            }
            "bin" -> {
                binaryLayout()
            }
        }
        btnSelected.isEnabled = false
        btnSelected.setBackgroundColor(Color.parseColor("#8122BD"))
    }

    /**
     * Sets the screen to match decimal layout.
     */
    private fun decimalLayout() {
        val txtDisabled = Color.parseColor("#999999")
        val backDisabled = Color.parseColor("#EEEEEE")
        val txtDefault = Color.parseColor("#FFFFFF")
        val backDefault = Color.parseColor("#D6D6D6")
        btn_Two.isEnabled = true
        btn_Two.setTextColor(txtDefault)
        btn_Two.setBackgroundColor(backDefault)
        btn_Three.isEnabled = true
        btn_Three.setTextColor(txtDefault)
        btn_Three.setBackgroundColor(backDefault)
        btn_Four.isEnabled = true
        btn_Four.setTextColor(txtDefault)
        btn_Four.setBackgroundColor(backDefault)
        btn_Five.isEnabled = true
        btn_Five.setTextColor(txtDefault)
        btn_Five.setBackgroundColor(backDefault)
        btn_Six.isEnabled = true
        btn_Six.setTextColor(txtDefault)
        btn_Six.setBackgroundColor(backDefault)
        btn_Seven.isEnabled = true
        btn_Seven.setTextColor(txtDefault)
        btn_Seven.setBackgroundColor(backDefault)
        btn_Eight.isEnabled = true
        btn_Eight.setTextColor(txtDefault)
        btn_Eight.setBackgroundColor(backDefault)
        btn_Nine.isEnabled = true
        btn_Nine.setTextColor(txtDefault)
        btn_Nine.setBackgroundColor(backDefault)
        btn_Period.isEnabled = true
        btn_Period.setTextColor(txtDefault)
        btn_Period.setBackgroundColor(backDefault)
        btn_Percent.isEnabled = true
        btn_Percent.setTextColor(txtDefault)
        btn_Percent.setBackgroundColor(backDefault)
        btn_ToggleSign.isEnabled = true
        btn_ToggleSign.setTextColor(txtDefault)
        btn_ToggleSign.setBackgroundColor(backDefault)
        btn_A.isEnabled = false
        btn_A.setTextColor(txtDisabled)
        btn_A.setBackgroundColor(backDisabled)
        btn_B.isEnabled = false
        btn_B.setTextColor(txtDisabled)
        btn_B.setBackgroundColor(backDisabled)
        btn_C.isEnabled = false
        btn_C.setTextColor(txtDisabled)
        btn_C.setBackgroundColor(backDisabled)
        btn_D.isEnabled = false
        btn_D.setTextColor(txtDisabled)
        btn_D.setBackgroundColor(backDisabled)
        btn_E.isEnabled = false
        btn_E.setTextColor(txtDisabled)
        btn_E.setBackgroundColor(backDisabled)
        btn_F.isEnabled = false
        btn_F.setTextColor(txtDisabled)
        btn_F.setBackgroundColor(backDisabled)
    }

    /**
     * Sets the screen to match hexadecimal layout.
     */
    private fun hexadecimalLayout() {
        val txtDisabled = Color.parseColor("#999999")
        val backDisabled = Color.parseColor("#EEEEEE")
        val txtDefault = Color.parseColor("#FFFFFF")
        val backDefault = Color.parseColor("#D6D6D6")
        btn_Two.isEnabled = true
        btn_Two.setTextColor(txtDefault)
        btn_Two.setBackgroundColor(backDefault)
        btn_Three.isEnabled = true
        btn_Three.setTextColor(txtDefault)
        btn_Three.setBackgroundColor(backDefault)
        btn_Four.isEnabled = true
        btn_Four.setTextColor(txtDefault)
        btn_Four.setBackgroundColor(backDefault)
        btn_Five.isEnabled = true
        btn_Five.setTextColor(txtDefault)
        btn_Five.setBackgroundColor(backDefault)
        btn_Six.isEnabled = true
        btn_Six.setTextColor(txtDefault)
        btn_Six.setBackgroundColor(backDefault)
        btn_Seven.isEnabled = true
        btn_Seven.setTextColor(txtDefault)
        btn_Seven.setBackgroundColor(backDefault)
        btn_Eight.isEnabled = true
        btn_Eight.setTextColor(txtDefault)
        btn_Eight.setBackgroundColor(backDefault)
        btn_Nine.isEnabled = true
        btn_Nine.setTextColor(txtDefault)
        btn_Nine.setBackgroundColor(backDefault)
        btn_A.isEnabled = true
        btn_A.setTextColor(txtDefault)
        btn_A.setBackgroundColor(backDefault)
        btn_B.isEnabled = true
        btn_B.setTextColor(txtDefault)
        btn_B.setBackgroundColor(backDefault)
        btn_C.isEnabled = true
        btn_C.setTextColor(txtDefault)
        btn_C.setBackgroundColor(backDefault)
        btn_D.isEnabled = true
        btn_D.setTextColor(txtDefault)
        btn_D.setBackgroundColor(backDefault)
        btn_E.isEnabled = true
        btn_E.setTextColor(txtDefault)
        btn_E.setBackgroundColor(backDefault)
        btn_F.isEnabled = true
        btn_F.setTextColor(txtDefault)
        btn_F.setBackgroundColor(backDefault)
        btn_Period.isEnabled = true
        btn_Period.setTextColor(txtDefault)
        btn_Period.setBackgroundColor(backDefault)
        btn_Percent.isEnabled = false
        btn_Percent.setTextColor(txtDisabled)
        btn_Percent.setBackgroundColor(backDisabled)
        btn_ToggleSign.isEnabled = false
        btn_ToggleSign.setTextColor(txtDisabled)
        btn_ToggleSign.setBackgroundColor(backDisabled)
        btn_Period.isEnabled = false
        btn_Period.setTextColor(txtDisabled)
        btn_Period.setBackgroundColor(backDisabled)
    }

    /**
     * Sets the screen to match binary layout.
     */
    private fun binaryLayout() {
        val txtDisabled = Color.parseColor("#999999")
        val backDisabled = Color.parseColor("#EEEEEE")
        btn_Two.isEnabled = false
        btn_Two.setTextColor(txtDisabled)
        btn_Two.setBackgroundColor(backDisabled)
        btn_Three.isEnabled = false
        btn_Three.setTextColor(txtDisabled)
        btn_Three.setBackgroundColor(backDisabled)
        btn_Four.isEnabled = false
        btn_Four.setTextColor(txtDisabled)
        btn_Four.setBackgroundColor(backDisabled)
        btn_Five.isEnabled = false
        btn_Five.setTextColor(txtDisabled)
        btn_Five.setBackgroundColor(backDisabled)
        btn_Six.isEnabled = false
        btn_Six.setTextColor(txtDisabled)
        btn_Six.setBackgroundColor(backDisabled)
        btn_Seven.isEnabled = false
        btn_Seven.setTextColor(txtDisabled)
        btn_Seven.setBackgroundColor(backDisabled)
        btn_Eight.isEnabled = false
        btn_Eight.setTextColor(txtDisabled)
        btn_Eight.setBackgroundColor(backDisabled)
        btn_Nine.isEnabled = false
        btn_Nine.setTextColor(txtDisabled)
        btn_Nine.setBackgroundColor(backDisabled)
        btn_A.isEnabled = false
        btn_A.setTextColor(txtDisabled)
        btn_A.setBackgroundColor(backDisabled)
        btn_B.isEnabled = false
        btn_B.setTextColor(txtDisabled)
        btn_B.setBackgroundColor(backDisabled)
        btn_C.isEnabled = false
        btn_C.setTextColor(txtDisabled)
        btn_C.setBackgroundColor(backDisabled)
        btn_D.isEnabled = false
        btn_D.setTextColor(txtDisabled)
        btn_D.setBackgroundColor(backDisabled)
        btn_E.isEnabled = false
        btn_E.setTextColor(txtDisabled)
        btn_E.setBackgroundColor(backDisabled)
        btn_F.isEnabled = false
        btn_F.setTextColor(txtDisabled)
        btn_F.setBackgroundColor(backDisabled)
        btn_Period.isEnabled = false
        btn_Period.setTextColor(txtDisabled)
        btn_Period.setBackgroundColor(backDisabled)
        /**/
        btn_Percent.isEnabled = false
        btn_Percent.setTextColor(txtDisabled)
        btn_Percent.setBackgroundColor(backDisabled)
        /**/
        btn_ToggleSign.isEnabled = false
        btn_ToggleSign.setTextColor(txtDisabled)
        btn_ToggleSign.setBackgroundColor(backDisabled)
    }

}