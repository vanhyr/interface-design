// ignore_for_file: prefer_const_constructors

import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calculator',
      home: const MyCalculator()
    );
  }
}

class MyCalculator extends StatefulWidget {
  const MyCalculator({Key? key}) : super(key: key);

  @override
  State<MyCalculator> createState() => _MyCalculatorState();
}

class _MyCalculatorState extends State<MyCalculator> {

  // Class variables for the result (the one that displays), the current operator
  // and if a period is present or not
  String result = "0";
  String operator = "";
  bool period = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // App background
      backgroundColor: Colors.black,
      appBar: AppBar(
        // Top bar background
        backgroundColor: Colors.black,
        // Top bar name to display
        title: Text("Calculator"),
      ),
      // Body
      body: Column(
        children: [
          // The calculator screen/display
          screen(),
          // The calculator buttons
          buttons()
        ]
      )
    );
  }

  // Widget for showing the screen for the calculator.
  Widget screen() {
    return Container(
      // Fill to the max width
      width: MediaQuery.of(context).size.width,
      height: 180,
      // Text alignment
      alignment: Alignment.bottomRight,
      // Background of the container
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 76, 76, 76),
      ),
      child:
        Padding(
          // Padding for right and bottom
          padding: const EdgeInsets.only(right: 18, bottom: 4),
          child: Text(
            // The text to display is the result of the calculator
            result,
            // Overflow text clips meaning can't write more than the container size
            overflow: TextOverflow.clip,
            // Max lines to show
            maxLines: 2,
            style: TextStyle(
              fontSize: 60,
              color: Colors.white,
            ),
            // Text align to the right
            textAlign: TextAlign.end,
          ),
        )
    );
  }

  // Widget for setting up buttons and their functionallity
  Widget buttons() {
    Color orange = Color.fromARGB(255, 170, 80, 8);    
    return Expanded(
      child: Padding(
        // Padding to just the top
        padding: const EdgeInsets.only(top: 10),
        // GridView for the buttons layout (contained inside an Expanded)
        child: GridView.count(
          //shrinkWrap: true,
          // How many columns to display in each row (horizontally)
          crossAxisCount: 4,
          children: [
            button("AC", orange),
            button("C"),
            button("%"),
            button("/", orange),
            button("7"),
            button("8"),
            button("9"),
            button("*", orange),
            button("4"),
            button("5"),
            button("6"),
            button("-", orange),
            button("1"),
            button("2"),
            button("3"),
            button("+", orange),
            button("0"),
            button("+/-"),
            button("."),
            button("=", orange),
          ],
        ),
      ),
    );
  }

  // Widget for creating buttons (grey default color, another can be passed as parameter)
  Widget button(String text, [Color color = const Color.fromARGB(255, 52, 52, 52)]) {
    return Padding(
      // Padding to all sides
      padding: const EdgeInsets.all(4.0),
      child: ElevatedButton(
        // Click event handler
        onPressed: () {
          switch (text) {
            // Calculates and sets the result
            case "=":
              equals();
              break;
            // Clears the screen
            case "AC":
              setState(() {
                // Clear the result (set to 0)
                result = "0";
                // Clear the operator
                operator = "";
                // Set the period to false
                period = false;
              });
              break;
            // Removes last digit
            case "C":
              // If there is an operation taking place
              if (result.isNotEmpty && result != "0") {
                // Extract the substring without the last digit
                String delete = result.substring(result.length - 1, result.length);
                //log(delete);
                // Check the delete digit and perform depending
                switch (delete) {
                  // If it is a period set period to false
                  case ".":
                    period = false;
                    break;
                  // If it is a / set operator to empty
                  case "/":
                    operator = "";
                    break;
                  // If it is a * set operator to empty
                  case "*":
                    operator = "";
                    break;
                    // If it is a - set operator to empty
                  case "-":
                    operator = "";
                    break;
                  // If it is a + set operator to empty
                  case "+":
                    operator = "";
                    break;
                  default:
                }
                // Delete last digit from the result
                setState(() {
                  result = result.substring(0, result.length - 1);
                });
              }
              break;
            // Percentage (divides by 100)
            case "%":
              if (result.isNotEmpty && result != "0") {
                if (operator.isEmpty) {
                  setState(() {
                    result = (double.parse(result) / 100).toString();
                  });
                } else {
                  String numA = result.substring(0, result.indexOf(operator));
                  String numB = result.substring(result.indexOf(operator) + 1, result.length);
                  if (numB.isNotEmpty) {
                    setState(() {
                      result = numA + operator + (double.parse(numB) / 100).toString();
                    });
                  }
                }
              }
              break;
            // Division
            case "/":
              checkOperator("/");
              break;
            // Multiplication
            case "*":
              checkOperator("*");
              break;
            // Sustract
            case "-":
              checkOperator("-");
              break;
            // Add
            case "+":
              checkOperator("+");
              break;
            // Negate sign
            case "+/-":
              if (result.isNotEmpty && result != "0") {
                if (operator.isEmpty) {
                  if (result.contains(".")) {
                    double num = double.parse(result);
                    num *= -1;
                    setState(() {
                      result = num.toString();
                    });
                  } else {
                    int num = int.parse(result);
                    num *= -1;
                    setState(() {
                      result = num.toString();
                    });
                  }
                } else {
                  //log(result.substring(0, result.indexOf(operator)));
                  //log(result.substring(result.indexOf(operator) + 1, result.length));
                  String numA = result.substring(0, result.indexOf(operator));
                  String numB = result.substring(result.indexOf(operator) + 1, result.length);
                  if (numB.isNotEmpty) {
                    if (result.contains(".")) {
                      double num = double.parse(numB);
                      num *= -1;
                      setState(() {
                        result = numA + operator + num.toString();
                      });
                    } else {
                      int num = int.parse(numB);
                      num *= -1;
                      setState(() {
                        result = numA + operator + num.toString();
                      });
                    }
                  }
                }
              }
              break;
            // Period
            case ".":
              if (!period) {
                if (result.isEmpty || result == "0") {
                  setState(() {
                    result = "0.";
                  });
                } else if (operator.isNotEmpty) {
                  //log(result.substring(result.indexOf(operator) + 1, result.length));
                  if (result.substring(result.indexOf(operator) + 1, result.length).isEmpty) {
                    setState(() {
                      result += "0" + text;
                    });
                  } else {
                    setState(() {
                      result += text;
                    });
                  }
                } else {
                  setState(() {
                    result += text;
                  });
                }
                period = true;
              }
              break;
            // Any other key just prints
            default:
              setState(() {
                // If there is no operation taking place
                if (result == "0") {
                  result = text;
                } else {
                  result += text;
                }
              });
          }
        },
        style: ElevatedButton.styleFrom(
          primary: color
        ),
        child: Text(
          // The pressed key
          text,
          style: TextStyle(fontSize: 24),
        )
      ),
    );
  }

  // Check if the operator is empty and the result doesn't end with a period.
  // If that is correct, adds the operator to the result and sets the operator to the new one.
  // Also sets period to false again
  checkOperator(operatorSign) {
    if (operator.isEmpty && !result.endsWith(".")) {
      setState(() {
        result += operatorSign;
        operator = operatorSign;
        period = false;
      });
    }
  }

  // Function for calculating the result using math expressions library
  equals() {
    try {
      // Creater the parser
      Parser parser = Parser();
      // Generate the expression from the given operation
      Expression expression = parser.parse(result);
      // Create a context model
      ContextModel contextModel = ContextModel();
      // Set eval to the result from the evaluation
      double eval = expression.evaluate(EvaluationType.REAL, contextModel);
      // Then set the result to the evaluation
      setState(() {
        result = eval.toString();
      });
      // Clear the operator
      operator = "";
      // Set period to false
      period = false;
    } catch(ex) {
      log(ex.toString());
    }
  }

}