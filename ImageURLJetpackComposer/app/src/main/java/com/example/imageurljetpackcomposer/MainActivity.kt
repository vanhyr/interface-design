package com.example.imageurljetpackcomposer

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.example.imageurljetpackcomposer.ui.theme.ImageURLJetpackComposerTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ImageURLJetpackComposerTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Greeting()
                }
            }
        }
    }
}

@Composable
fun loadImage(url : String) {
    Image(
        painter = rememberImagePainter(url),
        contentDescription = "image",
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)
            .height(150.dp)
            .clip(RoundedCornerShape(25.dp)),
        contentScale = ContentScale.FillWidth
    )
}

fun urlList(): Array<String> {
    return arrayOf(
        "https://siaguanta.com/wp-content/uploads/2020/05/que-es-wallpaper1.jpg",
        "https://www.solofondos.com/wp-content/uploads/2016/03/outrun-vaporwave-hd-wallpaper-preview.jpg",
        "https://cdn.hovia.com/app/uploads/Green-Tropical-Plant-Wallpaper-Mural-Plain.jpg",
        "https://resi.ze-robot.com/dl/4k/4k-desktop-wallpaper.-2560%C3%971080.jpg",
        "https://wallpapers.com/images/high/cool-mountain-and-stream-nature-pssp1epphoe7eluh.jpg",
        "https://wallpapers.com/images/high/cool-nature-colorful-sunset-va1brm4mhuxpzmq8.jpg"
    )
}

@Composable
fun Greeting() {
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        Text(
            text = "Image list by url",
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp, bottom = 20.dp),
            textAlign = TextAlign.Center,
            lineHeight = 32.sp,
            fontSize = 32.sp,
            color = Color.Magenta,
            fontWeight = FontWeight.Bold
        )
        LazyColumn(
            modifier = Modifier
                //.background(Color(24, 200, 45))
                .fillMaxSize()
                //.padding(top = 50.dp)
        ) {
            items(urlList()) {
                url ->
                //Box(modifier = Modifier.fillMaxWidth()) {
                    loadImage(url = url)
                //}
            }
        }
    }

}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ImageURLJetpackComposerTheme {
        Greeting()
    }
}