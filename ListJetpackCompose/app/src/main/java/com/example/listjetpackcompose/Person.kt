package com.example.listjetpackcompose

data class Person(
    val name : String,
    val surname : String,
    val telephone : String
)
