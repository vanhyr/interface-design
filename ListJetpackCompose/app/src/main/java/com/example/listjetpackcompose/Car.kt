package com.example.listjetpackcompose

data class Car(
    val brand : String,
    val model : String
)
