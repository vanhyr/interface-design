package com.example.listjetpackcompose

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.listjetpackcompose.ui.theme.ListJetpackComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ListJetpackComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    List()
                }
            }
        }
    }
}

@Composable
fun List() {
    val people : MutableList<Person> = mutableListOf()
    val cars : MutableList<Car> = mutableListOf()
    for (i in 1..100000) {
        people.add(Person("Name $i", "Surname $i", "$i"))
    }
    for (i in 1..100000) {
        cars.add(Car("Brand $i", "Model $i"))
    }
    LazyColumn(
        modifier = Modifier
            .fillMaxSize()
            //.background(color = Color.Red)
    ) {
        itemsIndexed(people) {
            position, person ->
                if (position % 2 == 0) {
                    ItemPerson(person = person)
                } else {
                    LazyRow {
                        items(cars) {
                            ItemCar(car = it)
                        }
                    }
                }
        }
    }
}

@Composable
fun ItemPerson(person: Person) {
    Box(
        modifier = Modifier
            .padding(10.dp)
            .clip(RoundedCornerShape(12))
            .background(color = Color.LightGray)
            .fillMaxWidth()
            .height(100.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
        ) {
            Text(
                text = person.name,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.ExtraBold
                )
            )
            Text(
                text = person.surname,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.ExtraBold
                )
            )
            Text(
                text = person.telephone,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.ExtraBold
                )
            )
        }
    }
}

@Composable
fun ItemCar(car: Car) {
    val context = LocalContext.current
    Box(
        modifier = Modifier
            .padding(10.dp)
            .clip(RoundedCornerShape(12))
            .background(color = Color.Blue)
            .fillMaxHeight()
            .width(100.dp)
            .clickable(onClick = {
                Toast.makeText(context, car.brand, Toast.LENGTH_LONG).show()
            })
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(10.dp)
        ) {
            Text(
                text = car.brand,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.ExtraBold
                )
            )
            Text(
                text = car.model,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight.ExtraBold
                )
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ListJetpackComposeTheme {
        List()
    }
}