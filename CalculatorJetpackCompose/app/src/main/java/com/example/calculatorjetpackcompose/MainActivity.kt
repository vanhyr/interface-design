package com.example.calculatorjetpackcompose

import android.content.res.Configuration
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.calculatorjetpackcompose.ui.theme.CalculatorJetpackComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CalculatorJetpackComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    Design()
                }
            }
        }
    }
}

@Composable
fun Design() {
    CalculatorJetpackComposeTheme {
        Row(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Column(modifier = Modifier
                .weight(1f)
            ) {
                DisplayText("Texto de prueba")
            }
        }
        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(4.dp)
                //.padding(top = 154.dp)
        ) {
            val orange = Color(247,92,31)
            Column(
                modifier = Modifier
                .weight(1f)
                .padding(4.dp)
            ) {
                Btn("C", Color.White, orange)
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("7")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("4")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("1")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("0")
            }
            Column(
                modifier = Modifier
                .weight(1f)
                .padding(4.dp)
            ) {
                Btn("+/-")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("8")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("5")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("2")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("")
            }
            Column(
                modifier = Modifier
                .weight(1f)
                .padding(4.dp)
            ) {
                Btn("%")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("9")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("6")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("3")
                Spacer(modifier = Modifier.padding(4.dp))
                Btn(".")
            }
            Column(
                modifier = Modifier
                .weight(1f)
                .padding(4.dp)
            ) {
                Btn("/", Color.White, orange)
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("*", Color.White, orange)
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("-", Color.White, orange)
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("+", Color.White, orange)
                Spacer(modifier = Modifier.padding(4.dp))
                Btn("=", Color.White, orange)
            }
        }
    }
}

@Composable
fun DisplayText(msg: String) {
    Text(
        modifier = Modifier
            .background(Color(76,76,76)),
            //.height(150.dp) ,
        text = msg,
        color = Color.White,
    )
}

@Composable
fun Btn(msg: String, color: Color = Color.Black, background: Color = Color(214,214,214)) {
    Button(
        modifier = Modifier
        .fillMaxWidth()
        .height(90.dp),
        colors = ButtonDefaults.buttonColors(backgroundColor = background),
        onClick = {
            //your onclick code here
    }) {
        Text(
            text = msg,
            color = color,
            fontWeight = FontWeight.Medium,
            fontSize = 24.sp,
        )
    }
}

@Preview(
    name = "Light Mode",
    showBackground = true,
    showSystemUi = true,
    uiMode = Configuration.UI_MODE_NIGHT_NO,
)
@Preview(
    name = "Dark Mode",
    showBackground = true,
    showSystemUi = true,
    uiMode = Configuration.UI_MODE_NIGHT_YES,
)
@Composable
fun DefaultPreview() {
    Design()
}