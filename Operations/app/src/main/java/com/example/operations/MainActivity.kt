package com.example.operations

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buttonAdd.setOnClickListener {
            add()
        }
        buttonMinus.setOnClickListener {
            minus()
        }
        buttonMultiply.setOnClickListener {
            multiply()
        }
        buttonDivide.setOnClickListener {
            divide()
        }
    }

    private fun add() {
        //textViewResult.text = ""
        val a = editTextNumberA.text.toString().toInt()
        val b = editTextNumberB.text.toString().toInt()
        val add = a + b
        textViewOperation.text = "+"
        textViewResult.text = add.toString()
    }

    private fun minus() {
        //textViewResult.text = ""
        val a = editTextNumberA.text.toString().toInt()
        val b = editTextNumberB.text.toString().toInt()
        val minus = a - b
        textViewOperation.text = "-"
        textViewResult.text = minus.toString()
    }

    private fun multiply() {
        //textViewResult.text = ""
        //textViewOperation.text = ""
        val a = editTextNumberA.text.toString().toInt()
        val b = editTextNumberB.text.toString().toInt()
        val multiply = a * b
        textViewOperation.text = "*"
        textViewResult.text = multiply.toString()
    }

    private fun divide() {
        //textViewResult.text = ""
        //textViewOperation.text = ""
        val a = editTextNumberA.text.toString().toInt()
        val b = editTextNumberB.text.toString().toInt()
        if (a > 0 && b > -1) {
            val division = a / b
            textViewOperation.text = "/"
            textViewResult.text = division.toString()
        }
    }

}