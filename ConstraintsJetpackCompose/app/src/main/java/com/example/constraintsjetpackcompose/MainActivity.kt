package com.example.constraintsjetpackcompose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.ConstraintSet
import androidx.constraintlayout.compose.Dimension
import com.example.constraintsjetpackcompose.ui.theme.ConstraintsJetpackComposeTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ConstraintsJetpackComposeTheme {
                // A surface container using the 'background' color from the theme
                Surface(color = MaterialTheme.colors.background) {
                    One()
                }
            }
        }
    }
}

@Composable
fun One() {
    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        Text(text = "Texto 1")
        Two()
        Text(text = "Texto 2")
    }
}

@Composable
fun Two() {
    val constraints = ConstraintSet() {
        val greenBox = createRefFor("greenBox")
        val redBox = createRefFor("redBox")
        val blueText = createRefFor("blueText")
        constrain(greenBox) {
            top.linkTo(parent.top, 10.dp)
            start.linkTo(parent.start)
            width = Dimension.value(150.dp)
            height = Dimension.value(150.dp)
        }
        constrain(redBox) {
            top.linkTo(greenBox.bottom, 40.dp)
            start.linkTo(parent.start, 50.dp)
            end.linkTo(parent.end, 40.dp)
            width = Dimension.value(150.dp)
            height = Dimension.value(150.dp)
        }
        constrain(blueText) {
            top.linkTo(redBox.bottom, 30.dp)
            start.linkTo(redBox.start)
        }
    }

    ConstraintLayout(constraints, modifier = Modifier.fillMaxSize()) {
        Box(
            modifier = Modifier
                .background(color = Color.Green)
                .layoutId("greenBox")
        ) {
            Text(text = "Green")
        }
        Box(
            modifier = Modifier
                .background(color = Color.Red)
                .layoutId("redBox")
        ) {
            Text(text = "Red")
        }
        Text(
            modifier = Modifier
                .layoutId("blueText"),
            text = "Blue"
        )
    }
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    ConstraintsJetpackComposeTheme {
        One()
    }
}