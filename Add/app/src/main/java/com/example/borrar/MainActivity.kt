package com.example.borrar

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button.setOnClickListener {
            sumar()
        }
    }

    fun sumar() {
        val suma = editTextNumberA.text.toString().toInt() +
                editTextNumberB.text.toString().toInt()
        textViewResultado.text = suma.toString()

    }
}