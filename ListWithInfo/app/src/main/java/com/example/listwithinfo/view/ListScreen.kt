package com.example.listwithinfo.view

import android.content.res.Configuration
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material.icons.filled.Star
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import com.example.listwithinfo.controller.Screen
import com.example.listwithinfo.controller.SeriesController
import com.example.listwithinfo.model.Serie
import com.example.listwithinfo.model.Character
import com.example.listwithinfo.ui.theme.ListWithInfoTheme

class ListScreen {

    private lateinit var navController: NavController
    private val lightGray: Color = Color(240, 240, 240)

    @ExperimentalMaterialApi
    @ExperimentalCoilApi
    @Composable
    fun ListScreen(navController: NavController) {
        this.navController = navController
        LoadList()
    }

    @ExperimentalMaterialApi
    @ExperimentalCoilApi
    @Composable
    private fun LoadList() {
        Scaffold(
            topBar = {
                TopAppBar(
                    backgroundColor = Color.LightGray,
                    contentColor = Color.Black,
                    title = {
                        Icon(
                            imageVector = Icons.Filled.Star,
                            contentDescription = "star",
                            modifier = Modifier.padding(end = 30.dp)
                        )
                        Text(
                            text = "Favourite series",
                            modifier = Modifier
                                .fillMaxWidth(),
                            fontSize = 24.sp,
                            fontWeight = FontWeight.Bold
                        )
                    }
                )
            },
            floatingActionButton = {
                FloatingActionButton(
                    onClick = {
                        // How to do go to top?
                    },
                    backgroundColor = lightGray,
                    contentColor = Color.Black
                ) {
                    Icon(
                        imageVector = Icons.Default.KeyboardArrowUp,
                        contentDescription = "go top"
                    )
                }
            }
        ) {
            LazyColumn(
                modifier = Modifier
                    .fillMaxSize()
                    .background(color = lightGray)
                    .padding(it),
                contentPadding = PaddingValues(vertical = 10.dp, horizontal = 5.dp)
            ) {
                items(
                    items = SeriesController.series,
                    key = {
                        serie -> serie.hashCode()
                    }
                ) {
                    serie ->
                        SwipeToDelete(serie)
                }
            }
        }
    }

    @ExperimentalMaterialApi
    @ExperimentalCoilApi
    @Composable
    private fun LoadSerie(serie: Serie) {
        Card(
            modifier = Modifier
                .padding(10.dp)
                .clip(RoundedCornerShape(3))
                .fillMaxWidth()
                .height(250.dp)
                .clickable {
                    navController.navigate(
                        Screen.Description.passArgs(
                            name = serie.name,
                            // If slashes are present (/) it doesn't work
                            // replacing with backslashes(\)
                            cover = serie.cover.replace("/", "\\"),
                            thumbnail = serie.thumbnail.replace("/", "\\"),
                            summary = serie.summary,
                            premiere = serie.premiere,
                            rating = serie.rating,
                            ended = serie.ended,
                            director = serie.director,
                            genre = serie.genre,
                            seasons = serie.seasons
                        )
                    )
                },
            backgroundColor = Color.LightGray,
            contentColor = Color.Black
        ) {
            Row {
                Surface(
                    modifier = Modifier
                        .width(160.dp)
                        .padding(top = 20.dp, bottom = 20.dp, start = 20.dp, end = 0.dp)
                        .clip(RoundedCornerShape(3.dp)),
                    shape = RectangleShape,
                    color = lightGray
                ) {
                    LoadImage(url = serie.cover, description = "cover")
                }
                Column(
                    modifier = Modifier
                        .padding(20.dp)
                        .fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = serie.name,
                        style = TextStyle(
                            fontSize = 22.sp,
                            fontWeight = FontWeight.SemiBold,
                        )
                    )
                    LazyRow(
                        verticalAlignment = Alignment.CenterVertically,
                        modifier = Modifier
                            .height(160.dp)
                            .fillMaxWidth()
                    ) {
                        if (serie.characters.isNotEmpty()) {
                            items(serie.characters) {
                                character ->
                                    LoadCharacter(character = character)
                            }
                        }
                    }
                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(
                            text = "\uD83D\uDCD6  " + serie.seasons + " seasons",
                            style = TextStyle(
                                fontSize = 16.sp
                            )
                        )
                        Text(
                            text = "\uD83C\uDF7F " + serie.rating,
                            style = TextStyle(
                                fontSize = 16.sp
                            )
                        )
                    }
                }
            }
        }
    }

    @ExperimentalCoilApi
    @Composable
    private fun LoadCharacter(character: Character) {
        Spacer(modifier = Modifier.width(21.dp))
        Surface(
            modifier = Modifier
                .size(120.dp)
                .clip(RoundedCornerShape(60.dp)),
            shape = CircleShape,
            color = lightGray,
        ) {
            LoadImage(url = character.avatar, description = "avatar")
        }
        Spacer(modifier = Modifier.width(21.dp))
    }

    @ExperimentalCoilApi
    @Composable
    private fun LoadImage(url: String, description: String = "image") {
        Image(
            painter = rememberImagePainter(
                data = url,
                builder = {
                    // cross fade animation between images
                    crossfade(true)
                }
            ),
            contentDescription = description,
            contentScale = ContentScale.Crop,
            alignment = Alignment.Center
        )
    }

    @ExperimentalCoilApi
    @ExperimentalMaterialApi
    @Composable
    private fun SwipeToDelete(serie: Serie) {
        val state = rememberDismissState(
            confirmStateChange = {
                if (it == DismissValue.DismissedToEnd) {
                    SeriesController.removeSerie(serie)
                }
                true
            }
        )
        SwipeToDismiss(
            state = state,
            background = {
                val color = when(state.dismissDirection) {
                    //DismissDirection.StartToEnd -> Color.Red
                    DismissDirection.StartToEnd -> Color(204,0,0)
                    DismissDirection.EndToStart -> Color.Transparent
                    null -> Color.Transparent
                }
                LoadDeleteBox(color)
            },
            dismissContent = {
                LoadSerie(serie)
            },
            directions = setOf(DismissDirection.StartToEnd)
        )
    }

    @Composable
    private fun LoadDeleteBox(color: Color) {
        Box(
            modifier = Modifier
                .padding(10.dp)
                .clip(RoundedCornerShape(3))
                .fillMaxWidth()
                .height(250.dp)
                .background(color)
        ) {
            Icon(
                imageVector = Icons.Default.Delete,
                contentDescription = "delete",
                tint = Color.White,
                modifier = Modifier
                    .align(Alignment.CenterStart)
                    .padding(start = 30.dp),
            )
        }
    }

    @ExperimentalMaterialApi
    @ExperimentalCoilApi
    @Preview(
        name = "Light Mode",
        showBackground = true,
        showSystemUi = true,
        uiMode = Configuration.UI_MODE_NIGHT_NO,
    )
    /*@Preview(
        name = "Dark Mode",
        showBackground = true,
        showSystemUi = true,
        uiMode = Configuration.UI_MODE_NIGHT_YES,
    )*/
    @Composable
    private fun DefaultPreview() {
        ListWithInfoTheme {
            ListScreen(navController = rememberNavController())
        }
    }

}