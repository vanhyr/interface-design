package com.example.listwithinfo.controller

sealed class Screen(val route: String) {

    object List: Screen("list_screen")
    object Description: Screen("description_screen/{name}/{cover}/{thumbnail}" +
        "/{summary}/{premiere}/{rating}/{ended}/{director}/{genre}/{seasons}") {
        fun passArgs(
            name: String,
            cover: String,
            thumbnail: String,
            summary: String,
            premiere: String,
            rating: Float,
            ended: Boolean,
            director: String,
            genre: String,
            seasons: Int,
        ): String {
            return "description_screen/$name/$cover/$thumbnail/$summary/$premiere" +
                    "/$rating/$ended/$director/$genre/$seasons"
        }
    }

}
