package com.example.listwithinfo.controller

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import coil.annotation.ExperimentalCoilApi
import com.example.listwithinfo.view.DescriptionScreen
import com.example.listwithinfo.view.ListScreen

@ExperimentalMaterialApi
@ExperimentalCoilApi
@Composable
fun SetUpNavGraph(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Screen.List.route
    ) {
        composable(
            Screen.List.route
        ) {
            ListScreen().ListScreen(navController = navController)
        }
        composable(
            Screen.Description.route,
            arguments = listOf(
                navArgument("name") {
                    type = NavType.StringType
                },
                navArgument("cover") {
                    type = NavType.StringType
                },
                navArgument("thumbnail") {
                    type = NavType.StringType
                },
                navArgument("summary") {
                    type = NavType.StringType
                },
                navArgument("premiere") {
                    type = NavType.StringType
                },
                navArgument("rating") {
                    type = NavType.FloatType
                },
                navArgument("ended") {
                    type = NavType.BoolType
                },
                navArgument("director") {
                    type = NavType.StringType
                },
                navArgument("genre") {
                    type = NavType.StringType
                },
                navArgument("seasons") {
                    type = NavType.IntType
                }
            )
        ) {
            navBackStackEntry ->
                DescriptionScreen().DescriptionScreen(
                    navBackStackEntry.arguments?.getString("name")!!,
                    navBackStackEntry.arguments?.getString("cover")!!,
                    navBackStackEntry.arguments?.getString("thumbnail")!!,
                    navBackStackEntry.arguments?.getString("summary")!!,
                    navBackStackEntry.arguments?.getString("premiere")!!,
                    navBackStackEntry.arguments?.getFloat("rating")!!,
                    navBackStackEntry.arguments?.getBoolean("ended")!!,
                    navBackStackEntry.arguments?.getString("director")!!,
                    navBackStackEntry.arguments?.getString("genre")!!,
                    navBackStackEntry.arguments?.getInt("seasons")!!,
                    navController = navController
                )
        }
    }
}