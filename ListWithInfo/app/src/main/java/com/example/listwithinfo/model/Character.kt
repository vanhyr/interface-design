package com.example.listwithinfo.model

data class Character(

    val name: String,
    val actor: String,
    val avatar: String,

)
