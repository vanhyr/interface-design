package com.example.listwithinfo.controller

import androidx.compose.runtime.mutableStateListOf
import com.example.listwithinfo.model.Character
import com.example.listwithinfo.model.Serie

// Could it be sealed?
class SeriesController {

    companion object {

        var series: MutableList<Serie> = mutableStateListOf(
            Serie(
                name = "Breaking Bad",
                cover = "https://resizing.flixster.com/Hr_3GpXR2tTBupgg68OqQShhDPc=/fit-in/" +
                        "1152x864/v2/https://flxt.tmsimg.com/assets/p185846_b_v9_ad.jpg",
                thumbnail = "https://wallpapercave.com/wp/0UKHkCb.jpg",
                summary = "When chemistry teacher Walter White is diagnosed with Stage III " +
                        "cancer and given only two years to live, he decides he has nothing to " +
                        "lose. He lives with his teenage son, who has cerebral palsy, and his " +
                        "wife, in New Mexico. Determined to ensure that his family will have a " +
                        "secure future, Walt embarks on a career of drugs and crime. He proves " +
                        "to be remarkably proficient in this new world as he begins " +
                        "manufacturing and selling methamphetamine with one of his former " +
                        "students.\n\nThe series tracks the impacts of a fatal diagnosis on " +
                        "a regular, hard working man, and explores how a fatal diagnosis affects " +
                        "his morality and transforms him into a major player of the drug trade.",
                premiere = "2008-01-20",
                rating = 9.4F,
                ended = true,
                director = "Vince Gilligan",
                genre = "Serial drama, Crime drama, Thriller, Neo-Western, Black comedy, Tragedy",
                seasons = 5,
                characters = mutableListOf(
                    Character(
                        name = "Walter White",
                        actor = "Brian Cranston",
                        avatar = "https://avatarfiles.alphacoders.com/252/252262.jpg",
                    ),
                    Character(
                        name = "Jessie Pinkman",
                        actor = "Aaron Paul",
                        avatar = "https://yt3.ggpht.com/a/AATXAJy-vRBerO7hd4WnvqB74SSWzMPvv-" +
                                "y5b45ueg=s900-c-k-c0xffffffff-no-rj-mo",
                    ),
                    Character(
                        name = "Skyler White",
                        actor = "Anna Gunn",
                        avatar = "https://static.wikia.nocookie.net/breakingbad/images/3/33/" +
                                "Skyler_S5b.jpg/revision/latest/scale-to-width-down/" +
                                "350?cb=20130717044318"
                    ),
                    Character(
                        name = "Hank Shcrader",
                        actor = "Dean Norris",
                        avatar = "https://static.wikia.nocookie.net/breakingbad/images/b/b7/" +
                                "HankS5.jpg/revision/latest/scale-to-width-down/750?cb=20120620014136"
                    ),
                    Character(
                        name = "Marie Schrader",
                        actor = "Betsy Brandt",
                        avatar = "https://static.wikia.nocookie.net/breakingbad/images/4/4b/" +
                                "MarieS5.jpg/revision/latest/scale-to-width-down/" +
                                "350?cb=20120620014705"
                    ),
                    Character(
                        name = "Gustavo Fring",
                        actor = "Giancarlo Esposito",
                        avatar = "https://static.wikia.nocookie.net/breakingbad/images/a/ab/" +
                                "BCS_S3_GusFringe.jpg/revision/latest/scale-to-width-down/" +
                                "350?cb=20170327185354"
                    ),
                    Character(
                        name = "Saul Goodman",
                        actor = "Bob Odenkirk",
                        avatar = "https://static.wikia.nocookie.net/breakingbad/images/5/56/" +
                                "BCS_S3_JimmyMcGill.jpg/revision/latest/scale-to-width-down/" +
                                "350?cb=20170327184952"
                    ),
                    Character(
                        name = "Mike Ehrmantraut",
                        actor = "Jonathan Banks",
                        avatar = "https://static.wikia.nocookie.net/breakingbad/images/8/8d/" +
                                "BCS_S3_MikeEhrmantraut.jpg/revision/latest/scale-to-width-down/" +
                                "350?cb=20170327185046"
                    ),
                )
            ),
            Serie(
                name = "Vikings",
                cover = "https://moviebird.com/wp-content/uploads/2018/06/Vikings.jpg",
                thumbnail = "https://wallpapercave.com/wp/wp2773656.jpg",
                summary = "Vikings is inspired by the sagas of Viking Ragnar Lothbrok, one " +
                        "of the best-known legendary Norse heroes and notorious as the scourge " +
                        "of England and France. The show portrays Ragnar as a farmer who rises " +
                        "to fame by successful raids into England, and eventually becomes a " +
                        "Scandinavian King, with the support of his family and fellow warriors." +
                        "\n\nIn the later seasons, the series follows the fortunes of his " +
                        "sons and their adventures in England, Scandinavia and the Mediterranean.",
                premiere = "2013-03-03",
                rating = 8.5F,
                ended = true,
                director = "Michael Hirts",
                genre = "Historical drama, Action-adventure",
                seasons = 6,
                characters = mutableListOf(
                    Character(
                        name = "Ragnar Sigurdsson",
                        actor = "Travis Fimmel",
                        avatar = "https://static.wikia.nocookie.net/vikingstv/images/8/86/" +
                                "Ragnar_Mid-Season_Promo.jpeg/revision/latest/" +
                                "scale-to-width-down/266?cb=20161219020239",
                    ),
                    Character(
                        name = "Lagertha Lothbrok",
                        actor = "Katheryn Winnick",
                        avatar = "https://static.wikia.nocookie.net/vikingstv/images/f/f4/" +
                                "LagerthaSeason6.jpeg/revision/latest/scale-to-width-down/" +
                                "327?cb=20201209232405"
                    ),
                    Character(
                        name = "Flóki Vilgerðarson",
                        actor = "Gustaf Skarsgård",
                        avatar = "https://static.wikia.nocookie.net/vikingstv/images/3/38/" +
                                "015f8840-4b35-11eb-8607-09e32f9ca1a5_1200_630.jpeg/revision/" +
                                "latest/scale-to-width-down/341?cb=20210102212801"
                    ),
                )
            ),
            Serie(
                name = "Narcos",
                cover = "https://dl9fvu4r30qs1.cloudfront.net/df/86/7dc536f14db89a17375b1fb7839d/" +
                        "juan-pablo-raba-in-narcos.jpg",
                thumbnail = "https://wallpapercave.com/wp/wp1811393.jpg",
                summary = "Narcos tells the true-life story of the growth and spread of " +
                        "cocaine drug cartels across the globe and attendant efforts of law " +
                        "enforcement to meet them head on in brutal, bloody conflict. It " +
                        "centers around the notorious Colombian cocaine kingpin Pablo Escobar " +
                        "(Wagner Moura) and Steve Murphy (Holbrook), a DEA agent sent to " +
                        "Colombia on a U.S. mission to capture him and ultimately kill him",
                premiere = "2015-08-28",
                rating = 8.8F,
                ended = true,
                director = "Chris Brancato, Carlo Bernard, Doug Miro",
                genre = "Crime drama, Biographical",
                seasons = 3,
                characters = mutableListOf(
                    Character(
                        name = "Pablo Escobar",
                        actor = "Wagner Moura",
                        avatar = "https://static.wikia.nocookie.net/narcos-mexico/images/9/9e/" +
                                "Escobar_surrender.png/revision/latest/scale-to-width-down/" +
                                "350?cb=20210602043923",
                    ),
                )
            ),
            Serie(
                name = "Peaky Blinders",
                cover = "https://vignette.wikia.nocookie.net/cinemorgue/images/9/93/" +
                        "Peaky-blinders-5277bd833cfd7.jpg/revision/latest?cb=20170311234242",
                thumbnail = "https://wallpapercave.com/wp/wp1890591.jpg",
                summary = "Birmingham, UK, 1919. In the aftermath of WW1, the Shelby family " +
                        "are making a name for themselves as bookmakers, racketeers and " +
                        "gangsters. Nominally the head of the family is the oldest brother, " +
                        "Arthur, but the real brains, ambition and drive in the organisation " +
                        "lies with Tommy, the second oldest. He will carve out an empire for " +
                        "himself that will stretch beyond Birmingham. This with the aid of his " +
                        "family and his gang, the Peaky Blinders.",
                premiere = "2013-09-12",
                rating = 8.8F,
                ended = false,
                director = "Steven Knight",
                genre = "Historical fiction, Crime drama",
                seasons = 6,
                characters = mutableListOf(
                    Character(
                        name = "Thomas Shelby",
                        actor = "Cillian Murphy",
                        avatar = "https://static.wikia.nocookie.net/peaky-blinders/images/8/8e/" +
                                "Tommys3.jpg/revision/latest/scale-to-width-down/" +
                                "333?cb=20190715140230",
                    ),
                    Character(
                        name = "Grace Helen",
                        actor = "Annabelle Wallis",
                        avatar = "https://static.wikia.nocookie.net/peaky-blinders/images/3/3b/" +
                                "Grace_Shelby.jpg/revision/latest/scale-to-width-down/" +
                                "333?cb=20171228170507",
                    ),
                    Character(
                        name = "Arthur Shelby",
                        actor = "Paul Anderson",
                        avatar = "https://static.wikia.nocookie.net/peaky-blinders/images/7/72/" +
                                "Arthur_Shelby_Series_5.jpg/revision/latest/" +
                                "scale-to-width-down/350?cb=20211027214616",
                    ),
                    Character(
                        name = "Jhon Shelby",
                        actor = "Joe Cole",
                        avatar = "https://static.wikia.nocookie.net/peaky-blinders/images/0/0b/" +
                                "Johns3.jpg/revision/latest/scale-to-width-down/" +
                                "333?cb=20190715140143",
                    ),
                    Character(
                        name = "Polly Gray",
                        actor = "Helen McCrory",
                        avatar = "https://static.wikia.nocookie.net/peaky-blinders/images/8/8a/" +
                                "Pollys4.jpg/revision/latest/scale-to-width-down/" +
                                "333?cb=20190713150345",
                    ),
                    Character(
                        name = "Michael Gray",
                        actor = "Finn Cole",
                        avatar = "https://static.wikia.nocookie.net/peaky-blinders/images/e/eb/" +
                                "Michael4.jpg/revision/latest/scale-to-width-down/" +
                                "333?cb=20171226144000",
                    ),
                )
            ),
            Serie(
                name = "Sons of Anarchy",
                cover = "https://mir-s3-cdn-cf.behance.net/project_modules/1400/" +
                        "5878d712181073.5625876ee112b.jpg",
                thumbnail = "https://www.hdwallpaper.nu/wp-content/uploads/2017/04/" +
                        "sons_of_anarchy-4.jpg",
                summary = "Sons of Anarchy is an adrenaline-charged initiation into the " +
                        "gritty underworld of outlaw motorcycle clubs. Hounded by law " +
                        "enforcement and targeted by ruthless rivals, the Sons of Anarchy " +
                        "face an uncertain and increasingly lawless future. Meanwhile, Jackson " +
                        "'Jax' Teller (Charlie Hunnam) is torn between loyalty to his brother" +
                        " bikers and the idealistic vision of his father as he faces bloodshed, " +
                        "treachery and betrayal. As he struggles to protect his family and " +
                        "escape the deadly legacy of the past, alliances are forged, secrets " +
                        "are revealed, and the bonds of brotherhood are put to the ultimate test.",
                premiere = "2008-09-03",
                rating = 8.5F,
                ended = true,
                director = "Kurt Sutter",
                genre = "Action, Crime drama, Tragedy, Neo-Western",
                seasons = 7,
                characters = mutableListOf(
                    Character(
                        name = "Jax Teller",
                        actor = "Charlie Hunnam",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/0/03/" +
                                "Jax_706.png/revision/latest/scale-to-width-down/" +
                                "350?cb=20141202055229",
                    ),
                    Character(
                        name = "Gemma Teller",
                        actor = "Katey Sagal",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/3/3e/" +
                                "GemmaTellerMorrow.jpg/revision/latest/scale-to-width-down/" +
                                "333?cb=20160403214640",
                    ),
                    Character(
                        name = "Tara Knowles",
                        actor = "Maggie Siff",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/0/0c/" +
                                "Tara_S6E13.png/revision/latest/scale-to-width-down/" +
                                "350?cb=20160129075805",
                    ),
                    Character(
                        name = "Opie Winston",
                        actor = "Ryan Hurst",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/2/24/" +
                                "Opie_411.jpg/revision/latest/scale-to-width-down/" +
                                "350?cb=20140822221130",
                    ),
                    Character(
                        name = "Clay Morrow",
                        actor = "Ron Perlman",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/1/14/" +
                                "Clay_511.jpg/revision/latest/scale-to-width-down/" +
                                "344?cb=20140822221341",
                    ),
                    Character(
                        name = "Bobby Munson",
                        actor = "Mark Boone",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/d/de/" +
                                "Bobby_706.png/revision/latest/scale-to-width-down/" +
                                "350?cb=20141202060038",
                    ),
                    Character(
                        name = "Tig Trager",
                        actor = "Kim Coates",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/c/c4/" +
                                "Tig_709.png/revision/latest/scale-to-width-down/" +
                                "350?cb=20141202060137",
                    ),
                    Character(
                        name = "Chibs Telford",
                        actor = "Tommy Flanagan",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/d/" +
                                "d1/Screenshot_2019-10-27-20-14-07~2.png/revision/latest/" +
                                "scale-to-width-down/350?cb=20191028032037",
                    ),
                    Character(
                        name = "Juice Ortiz",
                        actor = "Theo Rossi",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/d/d3/" +
                                "Juice_709.png/revision/latest/scale-to-width-down/" +
                                "350?cb=20141105031049",
                    ),
                    Character(
                        name = "Wayne Unser",
                        actor = "Dayton Callie",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/8/8d/" +
                                "Unser_702.png/revision/latest/scale-to-width-down/" +
                                "349?cb=20140916033745",
                    ),
                    Character(
                        name = "Nero Padilla",
                        actor = "Jimmy Smits",
                        avatar = "https://static.wikia.nocookie.net/sonsofanarchy/images/e/ed/" +
                                "Nero_704.png/revision/latest/scale-to-width-down/" +
                                "350?cb=20141001204426",
                    ),
                )
            ),
            Serie(
                name = "Mr. Robot",
                cover = "https://media.senscritique.com/media/000009877811/source_big/Mr_Robot.jpg",
                thumbnail = "https://wallpapercave.com/wp/wp1810627.jpg",
                summary = "Mr. Robot follows Elliot, a young programmer who works as a " +
                        "cyber-security engineer by day and as a vigilante hacker by night. " +
                        "Elliot finds himself at a crossroads when the mysterious leader of " +
                        "an underground hacker group recruits him to destroy the firm he is " +
                        "paid to protect.\n\nCompelled by his personal beliefs, Elliot " +
                        "struggles to resist the chance to take down the multinational CEOs he " +
                        "believes are running (and ruining) the world. Eventually, he realizes " +
                        "that a global conspiracy does exist, but not the one he expected.",
                premiere = "2017-10-11",
                rating = 8.5F,
                ended = true,
                director = "Sam Esmail",
                genre = "Drama, Techno-thriller, Psychological thriller",
                seasons = 7,
                characters = mutableListOf(
                    Character(
                        name = "Elliot Alderson",
                        actor = "Rami Malek",
                        avatar = "https://static.wikia.nocookie.net/mrrobot/images/3/3e/" +
                                "Elliot.jpg/revision/latest/scale-to-width-down/" +
                                "345?cb=20150810201239",
                    ),
                    Character(
                        name = "Mr. Robot",
                        actor = "Christian Slater",
                        avatar = "https://static.wikia.nocookie.net/mrrobot/images/4/40/" +
                                "Tumblr_9e63f38c6b4ab8b60b7af43ad8a40584_b5200ced_640.jpg/" +
                                "revision/latest/scale-to-width-down/350?cb=20190928213859",
                    ),
                    Character(
                        name = "Angela Moss",
                        actor = "Portia Doubleday",
                        avatar = "https://static.wikia.nocookie.net/mrrobot/images/4/4a/" +
                                "Tumblr_8a25712b9121eb8069ff575b842b9fc9_990d4100_1280.jpg/" +
                                "revision/latest/scale-to-width-down/350?cb=20190928214750",
                    ),
                    Character(
                        name = "Tyrell Wellick",
                        actor = "Martin Wallström",
                        avatar = "https://static.wikia.nocookie.net/mrrobot/images/1/1a/" +
                                "Mr.-Robot-1x04-3.jpg/revision/latest/scale-to-width-down/" +
                                "350?cb=20150725100044",
                    ),
                    Character(
                        name = "Darlene Alderson",
                        actor = "Carly Chaikin",
                        avatar = "https://static.wikia.nocookie.net/mrrobot/images/5/5c/" +
                                "Tumblr_96060a66d3af926ec3f638882b6ebb79_b2da2bc6_640.jpg/" +
                                "revision/latest/scale-to-width-down/350?cb=20190928213622",
                    ),
                )
            ),
        )

        fun removeSerie(serie: Serie): Boolean {
            return series.remove(serie)
        }

    }

}