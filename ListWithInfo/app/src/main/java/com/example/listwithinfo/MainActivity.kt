package com.example.listwithinfo

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.navigation.compose.rememberNavController
import coil.annotation.ExperimentalCoilApi
import com.example.listwithinfo.controller.SetUpNavGraph
import com.example.listwithinfo.ui.theme.ListWithInfoTheme

@ExperimentalMaterialApi
@ExperimentalCoilApi
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            ListWithInfoTheme {
                window.statusBarColor = Color.LightGray.toArgb()
                Surface(color = MaterialTheme.colors.background) {
                    SetUpNavGraph(navController = rememberNavController())
                }
            }
        }
    }

}