package com.example.listwithinfo.view

import android.content.res.Configuration
import androidx.compose.foundation.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
//import androidx.compose.ui.draw.blur
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.*
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import coil.annotation.ExperimentalCoilApi
import coil.compose.rememberImagePainter
import com.example.listwithinfo.controller.Screen
import com.example.listwithinfo.model.Serie
import com.example.listwithinfo.ui.theme.ListWithInfoTheme

class DescriptionScreen {

    private lateinit var navController: NavController
    private lateinit var serie: Serie
    private val lightGray: Color = Color(240, 240, 240)

    @ExperimentalCoilApi
    @Composable
    fun DescriptionScreen(
        name: String,
        cover: String,
        thumbnail: String,
        summary: String,
        premiere: String,
        rating: Float,
        ended: Boolean,
        director: String,
        genre: String,
        seasons: Int,
        navController: NavController
    ) {
        this.navController = navController
        serie = Serie(
            name,
            cover.replace("\\", "/"),
            thumbnail.replace("\\", "/"),
            summary,
            premiere,
            rating,
            ended,
            director,
            genre,
            seasons
        )
        LoadDescription()
    }

    @ExperimentalCoilApi
    @Composable
    private fun LoadDescription() {
        val scrollState = rememberScrollState()
        //val coroutineScope = rememberCoroutineScope()
        Scaffold(
            topBar = {
                TopAppBar(
                    backgroundColor = Color.LightGray,
                    contentColor = Color.Black,
                    title = {
                        Text(
                            text = serie.name,
                            modifier = Modifier
                                .fillMaxWidth(),
                            fontSize = 24.sp,
                            fontWeight = FontWeight.Bold
                        )
                    },
                    navigationIcon = {
                        IconButton(
                            onClick = {
                                navController.navigate(Screen.List.route)
                            }
                        ) {
                            Icon(
                                imageVector = Icons.Filled.ArrowBack,
                                contentDescription = "arrowBack"
                            )
                        }
                    }
                )
            },
            /*floatingActionButton = {
                FloatingActionButton(
                    onClick = {
                        coroutineScope.launch {
                            scrollState.animateScrollTo(0)
                        }
                    },
                    backgroundColor = Color.LightGray,
                    contentColor = Color.Black
                ) {
                    Icon(
                        imageVector = Icons.Default.KeyboardArrowUp,
                        contentDescription = "go top"
                    )
                }
            }*/
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(it)
                    .verticalScroll(scrollState)
                    .background(color = lightGray),
            ) {
                Box {
                    Surface(
                        modifier = Modifier
                            .fillMaxWidth()
                            .height(200.dp),
                        shape = RectangleShape,
                        color = Color.Gray,
                    ) {
                        LoadImage(url = serie.thumbnail, description = "thumbnail")
                    }
                    Surface(
                        modifier = Modifier
                            .width(130.dp)
                            .height(180.dp)
                            .offset(20.dp, 100.dp)
                            .clip(RoundedCornerShape(3.dp))
                            .border(2.5.dp, lightGray)
                        ,
                        shape = RectangleShape,
                        color = Color.LightGray,
                    ) {
                        LoadImage(url = serie.cover, description = "cover")
                    }
                    if (serie.ended) {
                        Text(
                            text = "\u2611\uFE0F",
                            modifier = Modifier
                                .offset(138.dp, 88.dp),
                            color = Color.Black
                        )
                    } else {
                        Text(
                            text = "\uD83D\uDD1C",
                            modifier = Modifier
                                .offset(138.dp, 88.dp),
                            color = Color.Black
                        )
                    }
                }
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 16.dp, start = 176.dp),
                    text = serie.name,
                    style = TextStyle(
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold,
                        textAlign = TextAlign.Left,
                        color = Color.Black
                    )
                )
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 6.dp, start = 176.dp, end = 6.dp),
                    text = serie.director,
                    style = TextStyle(
                        fontSize = 16.sp,
                        textAlign = TextAlign.Left,
                        color = Color.Gray
                    )
                )
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 30.dp, horizontal = 25.dp),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = "\uD83D\uDCD6  " + serie.seasons + " seasons",
                        style = TextStyle(
                            fontSize = 16.sp,
                            color = Color.Black
                        )
                    )
                    Text(
                        text = "\uD83D\uDCC5  " + serie.premiere,
                        style = TextStyle(
                            fontSize = 16.sp,
                            color = Color.Black
                        )
                    )
                    Text(
                        text = "\uD83C\uDF7F " + serie.rating,
                        style = TextStyle(
                            fontSize = 16.sp,
                            color = Color.Black
                        )
                    )
                }
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 25.dp, end = 25.dp, bottom = 30.dp),
                    text = serie.genre,
                    style = TextStyle(
                        fontSize = 16.sp,
                        fontStyle = FontStyle.Italic,
                        color = Color(86, 86, 86)
                    )
                )
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 20.dp, end = 20.dp, bottom = 20.dp),
                    text = "Summary: ",
                    style = TextStyle(
                        fontSize = 18.sp,
                        fontWeight = FontWeight.SemiBold,
                        color = Color.Black
                    )
                )
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 20.dp, end = 20.dp, bottom = 20.dp),
                    text = serie.summary,
                    style = TextStyle(
                        fontSize = 16.sp,
                        textAlign = TextAlign.Justify,
                        color = Color.Black
                    )
                )
            }
        }
    }

    @ExperimentalCoilApi
    @Composable
    private fun LoadImage(url: String, description: String = "image") {
        Image(
            painter = rememberImagePainter(
                data = url,
                builder = {
                    // cross fade animation between images
                    crossfade(true)
                }
            ),
            // Compose 1.1.0-alpha03 and kotlin 1.5.30 supports blur
            // in android 12 and above, lower android ignores blur
            /*modifier = Modifier
                .blur(radius = 200.dp),*/
            contentDescription = description,
            contentScale = ContentScale.Crop,
            alignment = Alignment.Center
        )
    }

    @ExperimentalCoilApi
    @Preview(
        name = "Light Mode",
        showBackground = true,
        showSystemUi = true,
        uiMode = Configuration.UI_MODE_NIGHT_NO,
    )
    /*@Preview(
        name = "Dark Mode",
        showBackground = true,
        showSystemUi = true,
        uiMode = Configuration.UI_MODE_NIGHT_YES,
    )*/
    @Composable
    private fun DefaultPreview() {
        ListWithInfoTheme {
            DescriptionScreen(
                name = "Breaking Bad",
                cover = "https://resizing.flixster.com/Hr_3GpXR2tTBupgg68OqQShhDPc=/fit-in/" +
                        "1152x864/v2/https://flxt.tmsimg.com/assets/p185846_b_v9_ad.jpg",
                thumbnail = "https://wallpapercave.com/wp/0UKHkCb.jpg",
                summary = "When chemistry teacher Walter White is diagnosed with Stage III " +
                        "cancer and given only two years to live, he decides he has nothing to " +
                        "lose. He lives with his teenage son, who has cerebral palsy, and his " +
                        "wife, in New Mexico. Determined to ensure that his family will have a " +
                        "secure future, Walt embarks on a career of drugs and crime. He proves" +
                        "to be remarkably proficient in this new world as he begins " +
                        "manufacturing and selling methamphetamine with one of his former " +
                        "students.\n\nThe series tracks the impacts of a fatal diagnosis on " +
                        "a regular, hard working man, and explores how a fatal diagnosis affects " +
                        "his morality and transforms him into a major player of the drug trade.",
                premiere = "2008-01-20",
                rating = 9.4F,
                ended = true,
                director = "Vince Gilligan",
                genre = "Serial drama, Crime drama, Thriller, Neo-Western, Black comedy, Tragedy",
                seasons = 5,
                navController = rememberNavController()
            )
        }
    }

}