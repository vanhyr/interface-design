package com.example.listwithinfo.model

data class Serie(

    val name: String,
    val cover: String,
    val thumbnail: String,
    val summary: String,
    val premiere: String,
    val rating: Float,
    val ended: Boolean,
    val director: String,
    val genre: String,
    val seasons: Int,
    val characters: MutableList<Character> = mutableListOf(),

)
