package com.example.navigationjetpack

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable

@Composable
fun setUpNavGraph(navController : NavHostController) {
    NavHost(
        navController = navController,
        startDestination = Screen.Home.route
    ) {
        composable(
            Screen.Home.route
        ) {
            homeScreen(navController = navController)
        }
        composable(
            Screen.Second.route
        ) {
            secondScreen()
        }
    }
}