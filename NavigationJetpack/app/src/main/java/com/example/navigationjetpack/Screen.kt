package com.example.navigationjetpack

sealed class Screen(val route : String) {
    object Home : Screen("home_screen")
    object Second : Screen("second_screen")
}